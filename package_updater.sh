#!/usr/bin/env bash
/home/lgblkb/.virtualenvs/lgblkb_tools/bin/python ./setup.py sdist bdist_wheel
/home/lgblkb/.virtualenvs/lgblkb_tools/bin/twine upload dist/*

. yaml.sh
create_variables package_info.yaml
#ip_address="10.10.13.16"
ip_address="178.91.253.19"
package_name="lgblkb-tools"
korsum_bin_dir=/home/lgblkb/.virtualenvs/korsum/bin
meteo_bin_dir=/home/lgblkb/.virtualenvs/Egistic_meteo/bin
server_bin_dir=/home/zhandos/backend/back_env/bin

update_with_pip () {
if [[ $1 -eq 0 ]]
then
	bin_dir=/home/lgblkb/.virtualenvs/korsum/bin
	${bin_dir}/python ${bin_dir}/pip install --upgrade ${package_name}==${version}
else
	ssh zhandos@${ip_address} ${2}/python ${2}/pip install --upgrade ${package_name}==${version}
fi
}

#update_with_pip 0 ${korsum_bin_dir}
#update_with_pip 0 ${korsum_bin_dir}
#update_with_pip 1 ${server_bin_dir}
