# from lgblkb_tools.log_support import create_logger
from lgblkb_tools import global_support as gsup
from celery import Celery
from lgblkb_tools import TheLogger,log_wrapper,setup_logger

simple_logger=TheLogger('lgblkb_logger')
# queuer=rmq_schedule.ConnectionManager().create_queue_manager()

# @queuer.with_receive('some_queue')
app=Celery(gsup.get_name(__file__),broker='pyamqp://guest@localhost//')

@app.task
def add(x,y):
	return x+y

def main():
	a=add.delay(2,2)
	print(a.get())
	pass

if __name__=='__main__':
	main()
