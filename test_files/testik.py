import logging

from lgblkb_tools import Folder,TheLogger

logs_folder=Folder('logs')
logger=TheLogger('lgblkb_logger2').stream_log(logging.INFO).file_log(logs_folder['logs.log'])

@logger.wrap()
def some_test():
	this_folder=Folder(__file__)
	qwe1_folder=this_folder['qwe1']
	root_folder=this_folder['qwe_0']
	qwe1_folder.copy_to(root_folder)
	zips_folder=this_folder['folder_with_zipped_folders']
	zip_filepath=root_folder.zip_to(zips_folder,'hmmm')
	logger.debug('zip_filepath: %s',zip_filepath)
	this_folder['folder_with_unzipped_folders'].unzip(zip_filepath)

	pass

def main():
	x='Salem'
	logger.debug('x: %s',x)
	logger.warning('Something important that is worth enough to be logged to stream)))')

	pass

if __name__=='__main__':
	main()
