import collections
import itertools
import os
from lgblkb_tools.global_support import *
from bs4 import BeautifulSoup
import requests
import codecs
import sys
from lgblkb_tools import Folder,setup_logger

log_folder=Folder('logs')
simple_logger=TheLogger('lgblkb_logger')

def step_1():
	words_filepath=r'/home/lgblkb/PycharmProjects/lgblkb_tools/test_files/words.txt'
	with open(words_filepath) as fh:
		lines=fh.readlines()
	
	next_counter=0
	counter=itertools.count(next_counter)
	for line in lines:
		line=line.strip()
		if not line: continue
		translation_item=line.split('\t')
		if len(translation_item)!=2: continue
		translation_item=[x.strip() for x in translation_item]
		if len(translation_item[0])>6: continue
		print(translation_item)
		next_counter=next(counter)
	simple_logger.debug('next_counter: %s',next_counter)
	words_per_day=next_counter/21
	simple_logger.debug('words_per_day: %s',words_per_day)

def main():
	words_filepath=r'/home/lgblkb/PycharmProjects/lgblkb_tools/test_files/selected_words.txt'
	with open(words_filepath) as fh:
		lines=fh.readlines()
	next_counter=0
	counter=itertools.count(next_counter)
	for line in lines:
		line=line.strip()
		if line[0]=='#': continue
		translation_item=eval(line)
		print(f"{translation_item[0]} - {translation_item[1]}")
		next_counter=next(counter)
	
	# simple_logger.debug('next_counter: %s',next_counter)
	# words_per_day=next_counter/21
	# simple_logger.debug('words_per_day: %s',words_per_day)
	
	# with open('words.txt','w') as fh:1
	# 	fh.write(soup.text)
	
	pass

if __name__=='__main__':
	main()
