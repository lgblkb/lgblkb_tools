import itertools
import time
from datetime import datetime,timedelta

import numpy as np
import pandas as pd

import lgblkb_tools.global_support as gsup
import lgblkb_tools.folder_utils
# from lgblkb_tools.log_support import log_wrapper,INFORM,create_logger
from lgblkb_tools.scheduling import dbader_schedule as schedule
from lgblkb_tools.scheduling.task_schedule import ScheduledTask
from lgblkb_tools import TheLogger,log_wrapper,setup_logger

simple_logger=TheLogger('lgblkb_logger')
log_folder=lgblkb_tools.folder_utils.Folder('logs')

# a=log_folder.create('folder1').create('folder2','folder3').get_filepath('qadfasd',include_depth=2,datetime_loc_index=2)
# print(a)
# simple_logger=create_logger(log_folder)

def cloud():
	np.random.seed(1)
	data_size=12
	df=pd.DataFrame(columns=['datetime','cloudiness'])
	df['cloudiness']=np.random.randint(0,100,data_size)
	df['datetime']=[datetime.fromtimestamp(datetime.now().timestamp()-timedelta(days=int(x)).total_seconds()) for x in
	                np.random.randint(0,14,data_size)]
	print(df)
	pass

counter=itertools.count()
status_getter=itertools.cycle([1,2,3])

class QWE(ScheduledTask):
	
	@log_wrapper()
	def setup(self,**kwargs):
		self.check_for(self.check1,schedule.every(3).seconds).do(self.process1,3)
	
	@log_wrapper()
	def check1(self):
		return True,'QWE'
	
	@log_wrapper()
	def process1(self,data):
		# simple_logger.add_file_handler()
		# self.log_create('process1').get_filepath(data=data).add_handler()
		simple_logger.info('QWE process1_data: %s',data)
		simple_logger.info('Started some long process: %s',data)
		time.sleep(70)
		simple_logger.info('Finished some long process: %s',data)
		
		return True,data

class Hoppa(ScheduledTask):
	
	def setup(self):
		self.check_for(self.check1,schedule.every(5).seconds).do(self.process1)
	
	@log_wrapper()
	def check1(self):
		return False,next(counter)
	
	@log_wrapper()
	def check2(self):
		return True,next(counter)
	
	def check3(self):
		return True,next(counter)
	
	@log_wrapper()
	def process1(self,data):
		simple_logger.info('process1_data: %s',data)
		time.sleep(10)
		return True,data+1000
	
	@log_wrapper()
	def process2(self,data):
		simple_logger.info('process2_data: %s',data)
	
	@log_wrapper()
	def process3(self,data):
		simple_logger.info('process3_data: %s',data)

def main():
	all_processes=ScheduledTask(filename=__file__)
	QWE(all_processes)
	# Hoppa(all_processes)
	all_processes.run()
	
	# simple_logger.create(__file__,'some_process').get_filepath(query_date='some_date').add_handler()
	# simple_logger.info('ok: %s','qwe')
	
	# # QWE(all_processes)
	# Hoppa(all_processes)
	# all_processes.run()
	# qwe=QWE(filename=__file__)
	# qwe.check_for(qwe.check1,schedule.every(3).seconds).do(qwe.process1,1)
	# qwe.run()
	# hoppa=Hoppa()
	# hoppa.check_for(hoppa.check1,schedule.every(2).seconds).do(hoppa.process1)
	# hoppa.check_for(hoppa.check1,schedule.every(2).seconds).check_for(hoppa.check2,schedule.every(5).seconds).do(hoppa.process1)
	# [print(x.control_desk) for x in hoppa.conveyors]
	# gsup.scheduled_task(__file__).run(hoppa,qwe)
	pass

if __name__=='__main__':
	main()
