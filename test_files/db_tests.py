import os
import socket

from box import Box
from sqlalchemy import Column,TIMESTAMP,Text,Integer

from lgblkb_tools import setup_logger,TheLogger
from lgblkb_tools.common_utils.statuses import image_status
from lgblkb_tools.db_utils import db_management as dbm
from lgblkb_tools.db_utils.sqla_orms import TextID_Base,Base,StatusTrackable

simple_logger=TheLogger('lgblkb_logger')
# simple_logger=create_logger('logs')
is_local_dev=os.environ.get('lgblkb') or socket.gethostname() in ['lgblkb-GT62VR-7RD']
configs=Box.from_yaml(filename='configs_lgblkb.yaml' if is_local_dev else 'configs.yaml').pg
mgr=dbm.Manager(configs['image_backend'],create_if_not_exists=True,database="temp_db3")

def test_1():
	# from db_utils.sqla_orms import Sentinel2_Info
	# from datetime import datetime
	# import pytz
	mgr.create_table(Sentinel2_Info)
	# with mgr.session_context() as session:
	# 	for i in range(100):
	# 		session.add(Sentinel2_Info(id=str(i),image_date=datetime.now(tz=pytz.utc),zip_path=__file__,priority=i))
	with mgr.session_context() as session:
		s2_info: Sentinel2_Info=session.query(Sentinel2_Info).get('36')
		print(s2_info)
	print(s2_info)
	s2_info: Sentinel2_Info=mgr.query_get(Sentinel2_Info,'36')
	print(s2_info)
	s2_info.priority=-123
	print(s2_info)
	# return
	with mgr.session_context() as session:
		s2_info=s2_info.reconnect(session)
		print(s2_info)
	print(s2_info)

@mgr.session_wrapper(False)
def test_2(session: dbm.orms.Session):
	s2_info: Sentinel2_Info=session.query(Sentinel2_Info).get('36')
	print(s2_info)
	# s2_info.priority=137
	
	pass

def test_3():
	from lgblkb_tools.scheduling.task_schedule import ScheduledTask,schedule
	class TheTask(ScheduledTask):
		
		def setup(self):
			self.check_for(self.check_1,schedule.every(2).seconds).do(self.do_something)
		
		@mgr.session_wrapper(False)
		def check_1(self,session: dbm.orms.Session):
			s2_info: Sentinel2_Info=session.query(Sentinel2_Info).first()
			
			return True,mgr.get_wrapped(s2_info)
		
		@mgr.session_wrapper()
		def do_something(self,session,data: Sentinel2_Info):
			x=data.priority
			print(data.priority)
			# data.priority=236
			print(data.priority)
			data=data.reconnect(session)
			for i in range(10000):
				data.status='Ura'
				data.priority=i
				data.product_path_l1c='some path'
			data.priority=x
			pass
	
	TheTask(filename=__file__).run()

def main():
	test_1()
	test_2()
	test_3()
	pass

if __name__=='__main__':
	main()

class Sentinel2_Info(TextID_Base,Base,StatusTrackable):
	__tablename__='sentinel2_info'
	image_date=Column(TIMESTAMP(timezone=True))
	download_date=Column(TIMESTAMP(timezone=True))
	product_path_l1c=Column(Text,server_default=None)
	zip_path=Column(Text)
	product_path_l2a=Column(Text,server_default=None)
	status=Column(Text,server_default=image_status.Unprocessed)
	priority=Column(Integer,default=5)
